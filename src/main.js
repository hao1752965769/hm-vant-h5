import Vue from 'vue'
import App from './App.vue'
import router from './router'

/*  */
// 引入vant全部组件
// import Vant from 'vant';
// 引入vant组件的样式
// import 'vant/lib/index.css';
// 将vant安装为vue的组件
// Vue.use(Vant);
import { Button } from 'vant'
Vue.use(Button)

Vue.config.productionTip = false
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
