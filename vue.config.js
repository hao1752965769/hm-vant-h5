const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  // 关闭所有eslint规则
  // 方式一：关闭所有eslint规则，及其报错信息
  // lintOnSave: false
  // 方式二：
  lintOnSave: true
})
